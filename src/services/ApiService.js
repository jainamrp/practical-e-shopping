import axios from "axios";

let endpoint = "https://dev-api.alldaydr.com/api/users";

const device_detail = {
  device_type: "web",
  player_id: "1",
};

const config = {
  "Content-Type": "application/json",
};

export function logout() {
  Object.keys(sessionStorage).forEach(function (storageKeys) {
    sessionStorage.removeItem(storageKeys);
  });
}

export function setAccessToken(cookie) {
  sessionStorage.setItem("access_token", JSON.stringify(cookie));
}

export function getAccessToken() {
  let tokenValue = JSON.parse(sessionStorage.getItem("access_token"));
  return tokenValue;
}

export function login(email, password) {
  const finalRequest = {
    user: {
      email: email,
      password: password,
    },
    device_detail: device_detail,
  };
  return axios({
    method: "post",
    url: endpoint + "/sign_in.json",
    data: finalRequest,
    config: config,
  })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
}

export function signup(payload) {
  const finalRequest = {
    user: payload,
    device_detail: device_detail,
  };
  return axios({
    method: "post",
    url: endpoint + "/sign_up.json",
    data: finalRequest,
    config: config,
  })
    .then((response) => {
      return response;
    })
    .catch((error) => {
      throw error;
    });
}

const ApiService = {
  setAccessToken,
  getAccessToken,
  login,
  signup,
  logout
};

export default ApiService;
