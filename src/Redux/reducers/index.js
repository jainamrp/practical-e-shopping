import { combineReducers } from "redux";
import loginReducer from "./login-reducer"
import cartReducer from "./cart-reducer";
import signupReducer from "./signup-reducer";
import inventoryReducer from "./inventory-reducer"

const rootReducer = combineReducers({
  loginReducer: loginReducer,
  cartReducer: cartReducer,
  signupReducer: signupReducer,
  inventoryReducer: inventoryReducer

});

export default (state, action) => {
  return rootReducer(state, action);
};
