import { ADD_PRODUCT_TO_INVENTORY } from "../actions/actionTypes";

export default function cartReducer(
  state = {
    inventoryData: []
  },
  action) {
  switch (action.type) {
    case ADD_PRODUCT_TO_INVENTORY:
      {
        return Object.assign({}, state, {
          inventoryData: action.data,
        });
      }
    default:
      return state;
  }
}
