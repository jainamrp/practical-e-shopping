import { SET_LOGIN_SUCCESS } from "../actions/actionTypes";

export default function loginReducer(
  state = {
    isLoginSuccess: false,
    data: undefined,
  },
  action
) {
  switch (action.type) {
    case SET_LOGIN_SUCCESS:
      return Object.assign({}, state, {
        isLoginSuccess: action.isLoginSuccess,
        data: action.data,
      });

    default:
      return state;
  }
}
