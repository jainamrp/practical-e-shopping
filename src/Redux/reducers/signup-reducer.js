import { SET_SIGNUP_SUCCESS } from "../actions/actionTypes";

export default function signupReducer(
  state = {
    isSignupSuccess: false,
    data: undefined,
  },
  action
) {
  switch (action.type) {
    case SET_SIGNUP_SUCCESS:
      return Object.assign({}, state, {
        isSignupSuccess: action.isSignupSuccess,
        data: action.data,
      });

    default:
      return state;
  }
}
