import { ADD_PRODUCT_TO_CART } from "../actions/actionTypes";

export default function cartReducer(
  state = {
    cartData: []
  },
  action) 

{
  switch (action.type) {
    case ADD_PRODUCT_TO_CART:
      {
        return Object.assign({}, state, {
          cartData: action.data,
        });
      }
    default:
      return state;
  }
}
