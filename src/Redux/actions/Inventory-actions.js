import { ADD_PRODUCT_TO_INVENTORY } from "../actions/actionTypes";

export function addProductToInventory(data) {
    return {
        type: ADD_PRODUCT_TO_INVENTORY,
        data: data
    };
}

export const addToInventory = (data) => {
    return (dispatch) => {
        dispatch(addProductToInventory(data));
    }

};













