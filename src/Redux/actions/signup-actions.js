import apiService from "../../services/ApiService";
import { SET_SIGNUP_SUCCESS } from "../actions/actionTypes";
import history from '../../history';
import { toast } from "react-toastify";

export function signupSuccess(isSuccess, data) {
  return {
    type: SET_SIGNUP_SUCCESS,
    isSignupSuccess: isSuccess,
    data: data,
  };
}

export const doSignup = (payload) => {
  return (dispatch) => {
    apiService.signup(payload).then(result => {
      if (result.data) {
        if (result.data.success === true) {
          if (result.data.data.user.auth_token) {
            apiService.setAccessToken(result.data.data.user.auth_token);
            dispatch(signupSuccess(true, result.data.data));
            history.push("/products-list");
            toast.success("Signup Success");
          } else {
            apiService.logout();
          }
        }
        else {
          if (result.data.status === 404) {
            toast.error(result.data.message);
          } else if (result.data.status === 500) {
            toast.error(result.data.message);
          } else if (result.data.status === 801) {
            toast.error(result.data.message);
          } else if (result.data.status === 805) {
            toast.error(result.data.message);
          }
          else {
            toast.error(result.data.message);
            toast.error("Something is wrong, Signup Failed");
          }
        }
      }
    }).catch((error) => {
      toast.error("Something is wrong, Signup Failed");
    });
  };
};
