import apiService from "../../services/ApiService";
import { SET_LOGIN_SUCCESS } from "../actions/actionTypes";
import history from '../../history';
import { toast } from "react-toastify";

export function loginSuccess(isSuccess, data) {
  return {
    type: SET_LOGIN_SUCCESS,
    isLoginSuccess: isSuccess,
    data: data,
  };
}

export const doLogin = (email, password) => {
  return (dispatch) => {
    apiService.login(email, password).then(result => {
      if (result.data) {
        if (result.data.status === 200 && result.data.success === true) {
          if (result.data.data.user.auth_token) {
            apiService.setAccessToken(result.data.data.user.auth_token);
            dispatch(loginSuccess(true, result.data.data));
            history.push("/products-list");
            toast.success("Login Success");
          } else {
            apiService.logout();
          }
        } else {
          if (result.data.status === 404) {
            toast.error(result.data.message);
          } else if (result.data.status === 500) {
            toast.error(result.data.message);
          } else if (result.data.status === 801) {
            toast.error(result.data.message);
          } else if (result.data.status === 808) {
            toast.error(result.data.message);
          }
          else {
            toast.error(result.data.message);
            toast.error("Something is wrong, login Failed");
          }
        }
      }
    }).catch((error) => {
      toast.error("Something is wrong, Signup Failed");
    });
  };
};
