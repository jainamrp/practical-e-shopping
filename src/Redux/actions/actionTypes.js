export const SET_LOGIN_SUCCESS = 'SET_LOGIN_SUCCESS';

export const SET_SIGNUP_SUCCESS = 'SET_SIGNUP_SUCCESS';

export const UPDATE_CART_DATA = "UPDATE_CART_DATA";

export const ADD_PRODUCT_TO_CART = "ADD_PRODUCT_TO_CART";

export const ADD_PRODUCT_TO_INVENTORY = "ADD_PRODUCT_TO_INVENTORY";