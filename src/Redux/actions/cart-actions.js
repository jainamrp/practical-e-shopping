import { ADD_PRODUCT_TO_CART } from "../actions/actionTypes";

export function addProduct(data) {
    return {
        type: ADD_PRODUCT_TO_CART,
        data: data
    };
}

export const addProductToCart = (data) => {
    return (dispatch) => {
        dispatch(addProduct(data));
    }

};













