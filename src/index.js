import React from "react";
import { render } from "react-dom";
import App from "./App";
import configureStore from "./Redux/store";
import { Provider } from "react-redux";
import "bootstrap/dist/css/bootstrap.min.css";

render(
  <Provider store={configureStore()}>
      <App />
  </Provider>,
  document.getElementById("root")
);
