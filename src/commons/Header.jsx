import React, { Component } from "react";
import CartLogo from "../commons/cart.png";
import history from '../history';
import * as ApiService from "../services/ApiService";
import { connect } from "react-redux";
import Button from "../components/ButtonInput";

class Header extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.onCartClick = this.onCartClick.bind(this);
    this.onLogoutClick = this.onLogoutClick.bind(this);
    this.onInventoryManager = this.onInventoryManager.bind(this);
  }

  onCartClick() {
    history.push("/cart-list");
  }

  onLogoutClick() {
    ApiService.logout();
    history.push("/login")
  }

  onInventoryManager() {
    history.push("/product-inventory");
  }

  render() {
    return (
      <div>

        <Button label={"Inventory Manager"} onClick={this.onInventoryManager}></Button>

        {(this.props.userdata && this.props.isLoginSuccess && !this.props.isSignupSuccess) ?
          <h3> {"Welcome  " + this.props.userdata.user.first_name + " " + this.props.userdata.user.last_name} </h3> : null
        }

        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

        {(this.props.signupuserdata && this.props.isSignupSuccess && !this.props.isLoginSuccess) ?
          <h3> {"Welcome  " + this.props.signupuserdata.user.first_name + " " + this.props.signupuserdata.user.last_name} </h3> : null
        }

        <img
          alt={""}
          src={CartLogo}
          height="42"
          width="42"
          onClick={this.onCartClick}
        ></img>

        {"                                                       "}
        <Button label={"Logout"} onClick={this.onLogoutClick}></Button>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    userdata: state.loginReducer.data,
    isLoginSuccess: state.loginReducer.isLoginSuccess,
    signupuserdata: state.signupReducer.data,
    isSignupSuccess: state.signupReducer.isSignupSuccess
  };
};

const mapDispatchToProps = () => {
  return {
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Header);