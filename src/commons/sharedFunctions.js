export const validateField = (valueToValidate, validationConfig, valueToCompare) => {
    let errorMessage = '';
    for (let i = 0; i < validationConfig.length; i++) {
        let validation = validationConfig[i];
        switch (validation.type) {
            case 'empty':
                errorMessage = valueToValidate.trim() ? '' : validation.errorMessage;
                break;
            case 'short':
                errorMessage = (valueToValidate.length < validation.length) ? validation.errorMessage : '';
                break;
            case 'long':
                errorMessage = (valueToValidate.length > validation.length) ? validation.errorMessage : '';
                break;
            case 'patternMatch':
                errorMessage = valueToValidate.match(validation.pattern) ? '' : validation.errorMessage;
                break;
            case 'compare':
                errorMessage = valueToCompare ? (valueToValidate === valueToCompare) ? '' : validation.errorMessage : '';
                break;
            case 'different':
                errorMessage = valueToCompare ? (valueToValidate !== valueToCompare) ? '' : validation.errorMessage : '';
                break;
            default:
                break;
        }
        if (errorMessage.length > 0) {
            return errorMessage;
        }
    }
}


export default validateField;