import React from "react";
import { Route, Switch } from "react-router-dom";
import Login from "./containers/Login";
import Signup from "./containers/Signup";
import ProductList from "./containers/ProductList";
import CartList from "./containers/CartList";
import Inventory from "./containers/Inventory"
import PageNotFound from "./components/PageNotFound";
import { Router } from "react-router-dom";
import history from "./history";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

function App() {
  return (
    <div>
      <ToastContainer autoClose={3000} />
      <Router history={history}>
        <Switch>
          <Route exact path="/" component={Signup} />
          <Route path="/login" component={Login} />
          <Route path="/signup" component={Signup} />
          <Route path="/products-list" component={ProductList} />
          <Route path="/cart-list" component={CartList} />
          <Route path="/product-inventory" component={Inventory} />
          <Route component={PageNotFound} />
          CartList
        </Switch>
      </Router>
    </div>
  );
}

export default App;
