import React, { Component } from "react";
import SignupTemplate from "../components/SignupTemplate";
import * as signupActions from "../Redux/actions/signup-actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import history from '../history';

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.signupCall = this.signupCall.bind(this);
    this.onLoginButtonClick = this.onLoginButtonClick.bind(this);
  }

  signupCall(params) {
    this.props.signupActions.doSignup(params);
  }

  onLoginButtonClick() {
    history.push("/login");
  }

  render() {
    return (
      <div>
        <SignupTemplate signupCall={this.signupCall} onLoginButtonClick={this.onLoginButtonClick} />
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    signupActions: bindActionCreators(signupActions, dispatch),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
