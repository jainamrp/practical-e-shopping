import React, { Component } from "react";
import InventoryTemplate from "../components/InventoryTemplate";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as inventoryActions from "../Redux/actions/Inventory-actions";
import * as ApiService from "../services/ApiService";
import history from "../history";

class Inventory extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
    this.IncreaseDecreaseQty = this.IncreaseDecreaseQty.bind(this);
    this.onBackButtonClick = this.onBackButtonClick.bind(this);
  }

  componentDidMount() {
    if (!ApiService.getAccessToken()) {
      history.push("/login");
    }
  }

  onBackButtonClick() {
    history.push("/products-list");
  }

  IncreaseDecreaseQty(_NewUpdatedData) {
    this.props.inventoryActions.addToInventory(_NewUpdatedData);
  }

  render() {
    return (
      <div>
        <InventoryTemplate
          onBackButtonClick={this.onBackButtonClick}
          inventoryData={this.props.inventoryData}
          IncreaseDecreaseQty={this.IncreaseDecreaseQty}
        />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    inventoryData: state.inventoryReducer.inventoryData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    inventoryActions: bindActionCreators(inventoryActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Inventory);
