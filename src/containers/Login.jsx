import React, { Component } from "react";
import LoginTemplate from "../components/LoginTemplate";
import * as loginActions from "../Redux/actions/login-actions";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.loginCall = this.loginCall.bind(this);
  }

  loginCall(params) {
    this.props.loginActions.doLogin(params.email, params.password);
  }

  render() {
    return (
      <div>
        <LoginTemplate loginCall={this.loginCall} />
      </div>
    );
  }
}

const mapStateToProps = () => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return {
    loginActions: bindActionCreators(loginActions, dispatch),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
