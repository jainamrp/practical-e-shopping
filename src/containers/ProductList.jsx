import React, { Component } from "react";
import ProductListTemplate from "../components/ProductListTemplate";
import Header from "../commons/Header";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cartActions from "../Redux/actions/cart-actions";
import * as ApiService from "../services/ApiService"
import history from '../history';
import * as inventoryActions from "../Redux/actions/Inventory-actions";
import products from "../commons/inventory";

class ProductList extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.callAddProductToCart = this.callAddProductToCart.bind(this);
  }

  componentDidMount() {
    if (!ApiService.getAccessToken()) {
      history.push("/login");
    }
    this.props.inventoryActions.addToInventory(products)
  }

  callAddProductToCart(data) {
    this.props.cartActions.addProductToCart(data);
  }

  render() {
    return (
      <div>
        <Header />
        <ProductListTemplate products={this.props.inventoryData} cartData={this.props.cartData} clickOnAddProductToCart={this.clickOnAddProductToCart} callAddProductToCart={this.callAddProductToCart} />
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartData: state.cartReducer.cartData,
    inventoryData: state.inventoryReducer.inventoryData
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cartActions: bindActionCreators(cartActions, dispatch),
    inventoryActions: bindActionCreators(inventoryActions, dispatch),
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductList);