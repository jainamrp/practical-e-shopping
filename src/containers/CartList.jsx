import React, { Component } from "react";
import CartListTemplate from "../components/CartListTemplate";
import products from "../commons/inventory";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as cartActions from "../Redux/actions/cart-actions";
import * as ApiService from "../services/ApiService";
import history from "../history";

class CartList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartData: this.props.cartData,
    };
    this.IncreaseDecreaseQty = this.IncreaseDecreaseQty.bind(this);
    this.onBackButtonClick = this.onBackButtonClick.bind(this);
  }

  componentDidMount() {
    if (!ApiService.getAccessToken()) {
      history.push("/login");
    }
  }

  onBackButtonClick() {
    history.push("/products-list");
  }

  IncreaseDecreaseQty(_NewUpdatedData) {
    this.props.cartActions.addProductToCart(_NewUpdatedData);
  }


  render() {
    return (
      <div>
        <CartListTemplate
          inventoryData={products}
          cartItems={this.state.cartData && this.state.cartData !== [] ? this.state.cartData : []}
          IncreaseDecreaseQty={this.IncreaseDecreaseQty}
          onBackButtonClick={this.onBackButtonClick}
        />
      </div>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    cartData: state.cartReducer.cartData,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    cartActions: bindActionCreators(cartActions, dispatch),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(CartList);
