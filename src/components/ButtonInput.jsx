import React from "react";
import PropTypes from "prop-types";

const ButtonInput = (props) => {
    return (
        <button
            id={props.id}
            className="btn btn-primary"
            type="button"
            onClick={props.onClick}
            name={props.name}
            disabled={(props.disabled) ? props.disabled : false}>
            {props.label}
        </button>
    );
};

ButtonInput.propTypes = {
    //id: PropTypes.string.isRequired,
    //name: PropTypes.string.isRequired,
    label: PropTypes.string.isRequired,
    onClick: PropTypes.func.isRequired,
};
ButtonInput.defaultProps = {
    disabled: false,
};

export default ButtonInput;
