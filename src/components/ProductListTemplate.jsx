import React, { Component } from "react";
import { toast } from "react-toastify";
import Button from "./ButtonInput";
import TableHeading from "./TableHeading";

class ProductListTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartdata: [],
    };
    this.clickOnAddProductToCart = this.clickOnAddProductToCart.bind(this);
    this.findProductByProductIdAndAdd = this.findProductByProductIdAndAdd.bind(this);
    this.verifyAlreadyExistProductInCart = this.verifyAlreadyExistProductInCart.bind(this);
  }

  clickOnAddProductToCart(productid) {
    if (this.verifyAlreadyExistProductInCart(productid)) {
      toast.error("Product is already in cart.");
    } else {
      const productData = this.findProductByProductIdAndAdd(productid);
      productData.qty = 1;
      let newcartData = [];
      if (this.props.cartData && this.props.cartData !== []) {
        this.props.cartData.map(Item => {
          newcartData.push(Item);
        });
        newcartData.push(productData);
        this.props.callAddProductToCart(newcartData);
      } else {
        newcartData.push(productData);
        this.props.callAddProductToCart(productData);
      }
      this.setState({ cartdata: productData })
    }
  }

  verifyAlreadyExistProductInCart(ItemId) {
    let avilablity = false;
    if (this.props.cartData)
      this.props.cartData.map((cartItem) => {
        if (ItemId === cartItem.id) {
          avilablity = true;
        }
      })
    return avilablity;
  }

  findProductByProductIdAndAdd(productid) {
    let __product = null;
    let _products = [];
    _products = JSON.parse(JSON.stringify(this.props.products));
    _products.map(_product => {
      if (_product.id === productid) {
        __product = _product;
      }
    })
    return __product;
  }

  render() {
    return (
      <div>
        <div className="jumbotron">
          <table className="table">
            <TableHeading />
            <tbody>
              {this.props.products.map((product) => {
                return (
                  <tr key={product.Name}>
                    <td> {product.Name}</td>
                    <td> {product.price}</td>
                    <td> {product.Location}</td>
                    <td> {product.qty}</td>
                    <td>
                      {(product.qty > 0) ?
                        (
                          <Button label={"Add to Cart"} onClick={() => this.clickOnAddProductToCart(product.id)} ></Button>
                        )
                        :
                        (
                          <Button label={"Sold Out"} disabled="true" ></Button>
                        )}
                    </td>
                  </tr>
                );
              })
              }
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default ProductListTemplate;
