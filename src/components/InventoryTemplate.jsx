import React, { Component } from "react";
import Button from "./ButtonInput";
import TableHeading from "./TableHeading";

class InventoryTemplate extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
        this.onBackButtonClick = this.onBackButtonClick.bind(this);
        this.onItemIncreaseDecrease = this.onItemIncreaseDecrease.bind(this);
        this.findItemandUpdateToInventory = this.findItemandUpdateToInventory.bind(this);
    }

    onBackButtonClick() {
        this.props.onBackButtonClick();
    }

    onItemIncreaseDecrease(ItemId, Operation) {
        this.findItemandUpdateToInventory(ItemId, Operation);
    }

    findItemandUpdateToInventory(ItemID, Operation) {
        let newInventoryData = this.props.inventoryData;
        this.props.inventoryData.map((Item) => {
            if (Item.id === ItemID) {
                if (Operation === "+") {
                    Item.qty = Item.qty + 1;
                } else {
                    Item.qty = Item.qty - 1;
                }
            }
        })
        this.setState({
            InventoryStock: newInventoryData
        }, () => {
            this.props.IncreaseDecreaseQty(newInventoryData);
        })
    }

    render() {
        return (
            <div>
                <Button id={"BackButton"} label={"Back"} onClick={this.onBackButtonClick} name={"BackButton"}></Button>
                <div>
                    <table className="table">
                        <TableHeading />
                        <tbody>
                            {this.props.inventoryData &&
                                this.props.inventoryData.map((Item) => {
                                    return (
                                        <tr key={Item.Name}>
                                            <td> {Item.Name}</td>
                                            <td> {Item.price}</td>
                                            <td> {Item.Location}</td>
                                            <td>
                                                {Item.qty === 0 ? (
                                                    <Button label={"-"} onClick={() => this.onItemIncreaseDecrease(Item.id, "-")} disabled="true" />
                                                ) : (
                                                        <Button label={"-"} onClick={() => this.onItemIncreaseDecrease(Item.id, "-")} />
                                                    )}
                                            </td>
                                            <td> {Item.qty}</td>
                                            <td>
                                                <Button label={"+"} onClick={() => this.onItemIncreaseDecrease(Item.id, "+")} />
                                            </td>
                                        </tr>
                                    );
                                })}
                        </tbody>
                    </table>
                </div>
            </div>
        );
    }
}

export default InventoryTemplate;
