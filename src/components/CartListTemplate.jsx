import React, { Component } from "react";
import { toast } from "react-toastify";
import Button from "./ButtonInput";
import TableHeading from "./TableHeading";

class CartListTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cartList: []
    };
    this.onItemIncreaseDecrease = this.onItemIncreaseDecrease.bind(this);
    this.onBackButtonClick = this.onBackButtonClick.bind(this);
    this.findItemandUpdateCartData = this.findItemandUpdateCartData.bind(this);
    this.findProducyByIdInInventory = this.findProducyByIdInInventory.bind(this);
  }

  componentDidMount() {
    this.props.cartItems.map((Item) => {
      this.state.cartList.push(Item);
    })
  }

  findProducyByIdInInventory(ItemID) {
    let productData = undefined;
    this.props.inventoryData.map((product) => {
      if (product.id === ItemID) {
        productData = product;
      }
    });
    return productData;
  }

  findItemandUpdateCartData(ItemID, Operation) {
    let _NewcartData = this.state.cartList;
    this.state.cartList.map(Item => {
      if (Item.id === ItemID) {
        _NewcartData.map(Item => {
          if (Item.id === ItemID) {
            if (Operation === "+") {
              let productdDetailfromInventoy = this.findProducyByIdInInventory(
                ItemID)
              if (productdDetailfromInventoy.qty > Item.qty) {
                Item.qty = Item.qty + 1;
              } else {
                toast.error("Item " + Item.Name + "is running out of stock.");
              }
            } else {
              Item.qty = Item.qty - 1;
            }
          }
        })
      }
    }
    )
    this.setState({
      cartList: _NewcartData
    }, () => {
      this.props.IncreaseDecreaseQty(_NewcartData);
    })
  }

  onItemIncreaseDecrease(ItemId, Operation) {
    this.findItemandUpdateCartData(ItemId, Operation);
  }

  onBackButtonClick() {
    this.props.onBackButtonClick();
  }


  render() {
    return (
      <div>
        <Button label={"Back"} onClick={this.onBackButtonClick} ></Button>
        <div>
          <table className="table">
            <TableHeading />
            <tbody>
              {this.props.cartItems &&
                this.props.cartItems.map((Item) => {
                  return (
                    <tr key={Item.Name}>
                      <td> {Item.Name}</td>
                      <td> {Item.price}</td>
                      <td> {Item.Location}</td>
                      <td>
                        {Item.qty === 1 ? (
                          <Button label={"-"} onClick={() =>
                            this.onItemIncreaseDecrease(Item.id, "-")
                          } disabled="true" ></Button>
                        ) : (
                            <Button label={"-"} onClick={() =>
                              this.onItemIncreaseDecrease(Item.id, "-")
                            }></Button>
                          )}
                      </td>
                      <td> {Item.qty}</td>
                      <td>
                        <Button label={"+"} onClick={() =>
                          this.onItemIncreaseDecrease(Item.id, "+")
                        } ></Button>
                      </td>
                    </tr>
                  );
                })}
            </tbody>
          </table>
        </div>
      </div>
    );
  }
}

export default CartListTemplate;
