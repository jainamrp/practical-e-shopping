import React from "react";

const TableHeading = () => {
    return (
        <tr>
            <th>Name </th>
            <th>Price </th>
            <th>Location </th>
            <th>Quantity </th>
        </tr>
    );
};

export default TableHeading;
