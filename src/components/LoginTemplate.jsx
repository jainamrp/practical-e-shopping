import React, { Component } from "react";
import TextInput from "./TextInput";
import Button from "./ButtonInput";
import validateField from "../commons/sharedFunctions";
import { validations } from "../App.Config";

class LoginTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      formErrors: {
        email: "",
        password: ""
      },
    };
    this.onChange = this.onChange.bind(this);
    this.onSubmitButtonClick = this.onSubmitButtonClick.bind(this);
    this.isAllFieldsValid = this.isAllFieldsValid.bind(this);
    this.validateField = this.validateField.bind(this);
  }

  onChange(event) {
    this.setState({ [event.target.name]: event.target.value });
  }

  onSubmitButtonClick() {
    if (this.isAllFieldsValid()) {
      const logindata = {
        email: this.state.email,
        password: this.state.password,
      };
      this.props.loginCall(logindata);
    }
  }

  isAllFieldsValid() {
    let formErrors = this.state.formErrors;
    let isAllValid = true;
    for (let fieldName in formErrors) {
      this.validateField(fieldName, this.state[fieldName]);
      if (formErrors[fieldName].length > 0) {
        isAllValid = false;
      }
    }
    return isAllValid;
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    switch (fieldName) {
      case 'email':
        fieldValidationErrors.email = validateField(value, validations["login.emailValidationConfig"]) ? validateField(value, validations["login.emailValidationConfig"]) : '';

        break;
      case 'password':
        let loginPasswordValidationConfig = [{ "type": "empty", "errorMessage": "Password is required" }];
        fieldValidationErrors.password = validateField(value, loginPasswordValidationConfig) ? validateField(value, loginPasswordValidationConfig) : '';
        break;

      default:
        break;
    }
    this.setState({ formErrors: fieldValidationErrors });
  }

  render() {
    return (
      <div>
        <TextInput
          id="email"
          label="Email"
          name="email"
          type="Text"
          value={this.state.email}
          onChange={this.onChange}
          error={this.state.formErrors.email}
        />
        <TextInput
          id="password"
          label="Password"
          name="password"
          type="Password"
          value={this.state.password}
          onChange={this.onChange}
          error={this.state.formErrors.password}
        />

        <Button label={"Login"} onClick={this.onSubmitButtonClick} ></Button>
      </div>
    );
  }
}

export default LoginTemplate;
