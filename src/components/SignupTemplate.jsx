import React, { Component } from "react";
import TextInput from "./TextInput";
import Button from "./ButtonInput";
import validateField from "../commons/sharedFunctions";
import { validations, NUMBER_PATTERN } from "../App.Config";

class SignupTemplate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      firstname: "",
      lastname: "",
      email: "",
      password: "",
      phone: "",
      formErrors: {
        firstname: "",
        lastname: "",
        email: "",
        password: "",
        phone: ""
      },
    };
    this.onChange = this.onChange.bind(this);
    this.onSignupButtonClick = this.onSignupButtonClick.bind(this);
    this.isAllFieldsValid = this.isAllFieldsValid.bind(this);
    this.onLoginButtonClick = this.onLoginButtonClick.bind(this);
    this.validateField = this.validateField.bind(this);
  }

  onChange(event) {
    if (event.target.name === "phone" && !event.target.value.match(NUMBER_PATTERN)) {
      if (event.target.value.length === 0) {
        this.setState({ [event.target.name]: event.target.value });
      }
    } else {
      this.setState({ [event.target.name]: event.target.value });
    }
  }

  onSignupButtonClick(event) {
    event.preventDefault();
    if (this.isAllFieldsValid()) {
      const payload = {};
      payload.first_name = this.state.firstname;
      payload.last_name = this.state.lastname;
      payload.email = this.state.email;
      payload.password = this.state.password;
      payload.phone = this.state.phone;
      this.props.signupCall(payload);
    }
  }

  onLoginButtonClick() {
    this.props.onLoginButtonClick();
  }

  isAllFieldsValid() {
    let formErrors = this.state.formErrors;
    let isAllValid = true;
    for (let fieldName in formErrors) {
      this.validateField(fieldName, this.state[fieldName]);
      if (formErrors[fieldName].length > 0) {
        isAllValid = false;
      }
    }
    return isAllValid;
  }

  validateField(fieldName, value) {
    let fieldValidationErrors = this.state.formErrors;
    switch (fieldName) {
      case 'firstname':
        let firstnameValidationConfig = [{ "type": "empty", "errorMessage": "FirstName is required" }];
        fieldValidationErrors.firstname = validateField(value, firstnameValidationConfig) ? validateField(value, firstnameValidationConfig) : '';
        break;

      case 'lastname':
        let lastnameValidationConfig = [{ "type": "empty", "errorMessage": "LastName is required" }];
        fieldValidationErrors.lastname = validateField(value, lastnameValidationConfig) ? validateField(value, lastnameValidationConfig) : '';
        break;

      case 'email':
        fieldValidationErrors.email = validateField(value, validations["login.emailValidationConfig"]) ? validateField(value, validations["login.emailValidationConfig"]) : '';
        break;

      case 'password':
        fieldValidationErrors.password = validateField(value, validations["login.passwordValidationConfig"]) ? validateField(value, validations["login.passwordValidationConfig"]) : '';
        break;

      case 'phone':
        fieldValidationErrors.phone = validateField(value, validations["signup.passwordValidationConfig"]) ? validateField(value, validations["signup.passwordValidationConfig"]) : '';
        break;

      default:
        break;
    }
    this.setState({ formErrors: fieldValidationErrors });
  }
  render() {
    return (
      <div>
        <TextInput
          id="firstname"
          label="FirstName"
          name="firstname"
          type="Text"
          value={this.state.firstname}
          onChange={this.onChange}
          error={this.state.formErrors.firstname}
        />
        <TextInput
          id="lastname"
          label="Lastname"
          name="lastname"
          type="Text"
          value={this.state.lastname}
          onChange={this.onChange}
          error={this.state.formErrors.lastname}
        />
        <TextInput
          id="email"
          label="Email"
          name="email"
          type="Text"
          value={this.state.email}
          onChange={this.onChange}
          error={this.state.formErrors.email}
        />
        <TextInput
          id="password"
          label="Password"
          name="password"
          type="Password"
          value={this.state.password}
          onChange={this.onChange}
          error={this.state.formErrors.password}
        />
        <TextInput
          id="phone"
          label="Contact No."
          name="phone"
          type="phone"
          value={this.state.phone}
          onChange={this.onChange}
          error={this.state.formErrors.phone}
        />
        <Button label={"Sign Up"} onClick={this.onSignupButtonClick} ></Button>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <Button label={"Login"} onClick={this.onLoginButtonClick} ></Button>
      </div>
    );
  }
}

export default SignupTemplate;
