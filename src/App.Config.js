export const NUMBER_PATTERN = /^[0-9\b]+$/;

export const EMAIL_PATTERN = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

export const validations = {
    "login.emailValidationConfig": [
        {
            "type": "empty",
            "errorMessage": "Email is required"
        },
        {
            "type": "long",
            "length": 50,
            "errorMessage": "Email is too long"
        },
        {
            "type": "patternMatch",
            "pattern": "^[A-Za-z0-9._%-+]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}$",
            "errorMessage": "Email is invalid"
        }
    ],
    "login.passwordValidationConfig": [
        {
            "type": "empty",
            "errorMessage": "Password is required"
        },
        {
            "type": "long",
            "length": 20,
            "errorMessage": "Password is too long"
        },
        {
            "type": "short",
            "length": 8,
            "errorMessage": "The Password must be at least 8 character long"
        },
        {
            "type": "patternMatch",
            "pattern": "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$",
            "errorMessage": "Password must have a number, a special character and one uppercase character"
        },
        {
            "type": "compare",
            "errorMessage": "Password does not match"
        }
    ],
    "signup.passwordValidationConfig": [
        {
            "type": "empty",
            "errorMessage": "Contact-Number is required"
        },
        {
            "type": "short",
            "length": 13,
            "errorMessage": "The Contact-Number must be at least 13 character long"
        },
        {
            "type": "patternMatch",
            "pattern": "^[0-9]+$",
            "errorMessage": "Invalid Contact-Number."
        }
    ],
}